import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'joshs-frogs-interview';
  validated = false;
  submitting = false;
  addressForm!: FormGroup;
  displayMessage = '';
  error = false;

  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {
    this.addressForm = new FormGroup({
      firstName: new FormControl(''),
      lastName: new FormControl(''),
      company: new FormControl(''),
      line1: new FormControl('', [Validators.required]),
      line2: new FormControl(''),
      city: new FormControl('', [Validators.required]),
      state: new FormControl('', [Validators.required, Validators.minLength(2)]),
      postalCode: new FormControl('', [Validators.required, Validators.minLength(5)]),
    });
  }

  validate() {
    if (!this.submitting) {
      this.resetDataForCall();
      if (this.addressForm.valid) {
        this.addressLookup();
      } else {
        this.submitting = false;
        this.validated = true;
      }
    }
  }

  resetDataForCall() {
    this.submitting = true;
    this.validated = false;
    this.error = false;
    this.displayMessage = '';
  }

  addressLookup() {
    const options = {headers: {accept: 'application/json', 'Content-Type': 'application/json'}};
    let shippingAddress = this.addressForm.value;
    shippingAddress.country = 'US';
    shippingAddress.residential = null;

    this.http.post('http://localhost:4200/api', shippingAddress, options).subscribe({
      next: data => {
        this.addressForm.patchValue(data);
        this.displayMessage = 'Successfully validated the address.';
        this.submitting = false;
      },
      error: error => {
        this.handleError(error);
        this.error = true;
        this.submitting = false;
      }
    })
  }

  handleError(error: any) {
    if (error.status == 500) {
      this.displayMessage = 'Failed to locate the address. Please try again.';
    } else {
      this.displayMessage = 'Something went wrong on our end. Please try again later.';
    }
  }

  get line1() {
    return this.addressForm.get('line1');
  }

  get city() {
    return this.addressForm.get('city');
  }

  get state() {
    return this.addressForm.get('state');
  }

  get postalCode() {
    return this.addressForm.get('postalCode');
  }
}
