import {TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppComponent} from './app.component';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('AppComponent', () => {
  let app: AppComponent;
  let httpMock: HttpTestingController;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
    httpMock = TestBed.inject(HttpTestingController);
    const fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;
  });

  it('should create the app', () => {
    expect(app).toBeTruthy();
  });

  it(`should have as title 'joshs-frogs-interview'`, () => {
    expect(app.title).toEqual('joshs-frogs-interview');
  });

  it('ngOnInit should define addressForm', () => {
    expect(app.addressForm).toBeFalsy();
    app.ngOnInit();

    expect(app.addressForm).toBeTruthy();
  });

  it('validate does nothing if already submitting', () => {
    app.submitting = true;
    spyOn(app, 'resetDataForCall');
    app.validate();

    expect(app.resetDataForCall).not.toHaveBeenCalled();
  });

  it('validate succeeds and calls addressLookup', () => {

    spyOn(app, 'resetDataForCall');
    spyOn(app, 'addressLookup');
    app.ngOnInit();
    app.addressForm.patchValue({line1: 'address', city: 'city', postalCode: 'postalCode', state: 'state'});
    app.validate();

    expect(app.resetDataForCall).toHaveBeenCalled();
    expect(app.addressLookup).toHaveBeenCalled();
  });

  it('validate fails and sets validated', () => {
    spyOn(app, 'resetDataForCall');
    spyOn(app, 'addressLookup');
    app.ngOnInit();
    app.validate();

    expect(app.resetDataForCall).toHaveBeenCalled();
    expect(app.addressLookup).not.toHaveBeenCalled();
    expect(app.validated).toBeTrue();
    expect(app.submitting).toBeFalse();
  });

  it('resetDataForCall successfully sets up variables', () => {
    app.submitting = false;
    app.validated = true;
    app.error = true;
    app.displayMessage = 'test';
    app.resetDataForCall();

    expect(app.submitting).toBeTrue();
    expect(app.validated).toBeFalse();
    expect(app.error).toBeFalse();
    expect(app.displayMessage).toEqual('');
  });

  it('addressLookup succeeds', () => {
    app.ngOnInit();
    app.submitting = true;
    app.addressForm.patchValue({line1: 'address', city: 'city', postalCode: 'postalCode', state: 'state'});

    app.addressLookup();
    const req = httpMock.expectOne('http://localhost:4200/api');
    expect(req.request.method).toBe('POST');
    expect(req.request.body).toEqual({
      firstName: '',
      lastName: '',
      company: '',
      line1: 'address',
      line2: '',
      city: 'city',
      state: 'state',
      postalCode: 'postalCode',
      country: 'US',
      residential: null
    });

    req.flush({line1: 'address', city: 'city', postalCode: 'postalCode', state: 'state'});

    expect(app.submitting).toBeFalse();
    expect(app.displayMessage).toEqual('Successfully validated the address.');
  });

  it('addressLookup fails', () => {
    app.ngOnInit();
    app.submitting = true;
    app.addressForm.patchValue({line1: 'address', city: 'city', postalCode: 'postalCode', state: 'state'});
    spyOn(app, 'handleError');

    app.addressLookup();
    const req = httpMock.expectOne('http://localhost:4200/api');
    expect(req.request.method).toBe('POST');
    expect(req.request.body).toEqual({
      firstName: '',
      lastName: '',
      company: '',
      line1: 'address',
      line2: '',
      city: 'city',
      state: 'state',
      postalCode: 'postalCode',
      country: 'US',
      residential: null
    });
    req.flush(null, {status: 500, statusText: 'Address lookup failure.'});

    expect(app.error).toBeTrue();
    expect(app.submitting).toBeFalse();
    expect(app.handleError).toHaveBeenCalled();
  });

  it('handleError tells a user when the address lookup failed', () => {
    const error = {status: 500};
    app.handleError(error);

    expect(app.displayMessage).toEqual('Failed to locate the address. Please try again.');
  });

  it('handleError tells a user when something went wrong like a timeout', () => {
    const error = {status: 408};
    app.handleError(error);

    expect(app.displayMessage).toEqual('Something went wrong on our end. Please try again later.');
  });

  it('line1 returns the FormControl', () => {
    app.ngOnInit();
    const formControl = app.line1;

    expect(formControl).toBeTruthy();
  });

  it('city returns the FormControl', () => {
    app.ngOnInit();
    const formControl = app.city;

    expect(formControl).toBeTruthy();
  });

  it('state returns the FormControl', () => {
    app.ngOnInit();
    const formControl = app.state;

    expect(formControl).toBeTruthy();
  });

  it('postalCode returns the FormControl', () => {
    app.ngOnInit();
    const formControl = app.postalCode;

    expect(formControl).toBeTruthy();
  });
});
